const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const firebase = require('firebase');
const config = {
  apiKey: "AIzaSyAAZ9e48GBJ25AYMPxHDJh38MT1B6oKdHE",
  authDomain: "signal-mutual-resources.firebaseapp.com",
  databaseURL: "https://signal-mutual-resources.firebaseio.com",
  projectId: "signal-mutual-resources",
  storageBucket: "signal-mutual-resources.appspot.com",
  messagingSenderId: "760446275228",
  keyFilename: "Signal Mutual Resources-db041140228c.json"
};
firebase.initializeApp(config);

const index = require('./routes/index');
const login = require('./routes/login');
const logout = require('./routes/logout');
const video = require('./routes/video');
const website = require('./routes/website');
const doc = require('./routes/doc');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));

app.use('/', index);
app.use('/login', login);
app.use('/video', video);
app.use('/website', website);
app.use('/document', doc);
app.use('/logout', logout);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
