const pdfInfo = require('pdfinfo');
//const exif = require('exiftool');
const fs = require('fs');

export default function getPdfInfo(filePath: string): Promise<any> {
    const pdf = pdfInfo(filePath);
    return new Promise((resolve, reject) => {
        pdf.info((err: any, meta: any) => {
            if (err) reject(err);
            else resolve(meta);
        })
    });
}

// export default function getPdfInfo(filePath: string): Promise<any> {
//     return new Promise((resolve, reject) => {
//         fs.readFile(filePath, function (err: any, data: any) {
//             if (err)
//                 reject(err);
//             else {
//                 exif.metadata(data, function (err: any, metadata: any) {
//                     console.log(metadata);
//                     if (err) reject(err);
//                     else resolve(metadata);
//                 });
//             }
//         });
//     });
// }