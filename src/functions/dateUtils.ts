export function getMonthNumber(month: string): number {
    const lowercaseMonth = month.toLowerCase();

    interface monthMap {
        [key: string]: number
    }

    const months: monthMap = {
        jan: 0,
        feb: 1,
        mar: 2,
        apr: 3,
        may: 4,
        jun: 5,
        jul: 6,
        aug: 7,
        sep: 8,
        oct: 9,
        nov: 10,
        dec: 11,
    };
    return months[lowercaseMonth];
}

export function dateToMilliseconds(dateString: string): number {
    try {
        const date = dateString.split(' ');
        const year = date[date.length - 1];
        const month = date[1];
        let day;
        if (date[2] === '') day = date[3];
        else day = date[2];

        const dateObj = new Date(parseInt(year), getMonthNumber(month), parseInt(day), 0, 0, 0, 0);
        return dateObj.getTime();
    }
    catch (error) {
        return new Date().getTime();
    }
}