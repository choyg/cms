import getEncodedPath from "./getEncodedPath";

export default function getFirebaseDownloadURL(file: Express.Multer.File): string {
    const token = file.filename;
    const path = getEncodedPath(file);
    return `https://firebasestorage.googleapis.com/v0/b/signal-mutual-resources.appspot.com/o/${path}?alt=media&token=${token}`;
}