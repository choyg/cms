import * as Scissors from "scissors";
import * as fs from 'fs';
import * as ulid from 'ulid';
import * as path from 'path';

/**
 * Takes pdf and returns the first page1
 * @param path Path to given PDF
 * @returns {Promise<string>} resolve: Path to PDF first page
 */
export default function getPdfFirstpage(path: string): Promise<string> {
    return new Promise(((resolve, reject) => {
        const key = ulid();
        const pagePath = `./thumbnails/${key}.pdf`;
        ensureDirectoryExistence(pagePath);
        Scissors(path)
            .pages(1)
            .pdfStream()
            .pipe(fs.createWriteStream(pagePath))
            .on('finish', function () {
                resolve(pagePath);

            }).on('error', function (err: Error) {
                unlinkFile(pagePath);
                console.log(err);
                reject(err);
            });
    }));
}

/**
 * Synchronously unlink
 * @param path
 */
function unlinkFile(path: string) {
    fs.unlinkSync(path);
}
/**
 * Synchronously create a directory if it does not exist
 */
function ensureDirectoryExistence(filePath: string) {
  var dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}