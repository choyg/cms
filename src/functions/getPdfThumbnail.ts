import * as EasyImage from 'easyimage';
import * as ulid from 'ulid';
import * as fs from 'fs';
import getPdfFirstPage from "./getPdfFirstPage";

export default function getPdfThumbnail(path: string): Promise<any> {
    let mPdfPath: string;
    return new Promise((resolve, reject) => {
        getPdfFirstPage(path)
            .then((pdfPath) => {
                mPdfPath = pdfPath;
                return EasyImage.convert({
                    src: pdfPath,
                    dst: `./thumbnails/${ulid()}.jpg`,
                    quality: 50,
                })
            })
            .then((imgObj) => {
                try {
                    fs.unlinkSync(mPdfPath) // delete first pdf page
                } catch (e) {
                    console.log(e);
                }
                resolve(imgObj);
            })
            .catch((err) => {
                try {
                    fs.unlinkSync(mPdfPath) // delete first pdf page
                } catch (e) {
                    console.log(e);
                }
                reject(err);
            })
    })
}