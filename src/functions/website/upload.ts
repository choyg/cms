import {error} from "util";

const firebase = require('firebase');
const database = firebase.database();

export function addWebsite(url: string,
                           title: string,
                           summary: string,
                           category: string): String {
    return pushToFirebase(url, title, summary, category);
}

function pushToFirebase(url: string,
                        title: string,
                        summary: string,
                        category: string): String {
    const date = new Date().getTime();
    const fileData = {
        title: title,
        summary: summary,
        mimetype: 'link',
        path: url,
        date: date
    };
    const path = 'resources/' + category;
    const fileRef = database.ref(path);
    fileRef.push(fileData, (error: any) => {
        console.log(error);
    });
    return fileRef.key;
}