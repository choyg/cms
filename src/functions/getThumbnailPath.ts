export default function getThumbnailPath(file: Express.Multer.File) {
    return file.filename + "/thumbnail.jpg";
}