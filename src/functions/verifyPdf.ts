export default function verifyPdf(file: Express.Multer.File): Boolean {
    const isPdf = (file.mimetype === 'application/pdf');
    console.log(file.mimetype);
    if (!isPdf) console.log("Wrong filetype detected");
    return isPdf;
}