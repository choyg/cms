export default function getThumbnailEncodedPath(file: Express.Multer.File): string {
    return file.filename + "%2Fthumbnail.jpg";
}