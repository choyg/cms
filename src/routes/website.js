const express = require('express');
const router = express.Router();
const firebase = require('firebase');
const website = require('../functions/website/upload');

function validateCategory(category) {
  switch (category) {
    case 'training':
      return true;
    case 'safety':
      return true;
    case 'policy':
      return true;
    case 'resources':
      return true;
    case 'checklists':
      return true;
    case 'links':
      return true;
    case 'extras':
      return true;
    case 'miscellaneous':
      return true;
    default:
      return false;
  }
}

/* GET home page. */
router.get('/', (req, res) => {
  const user = firebase.auth().currentUser;
  if (user) {
    res.render('website', { title: 'Add Website' });
  } else {
    res.redirect('/login');
  }
});

router.post('/', async (req, res) => {
  const user = firebase.auth().currentUser;
  if (!user) {
    res.redirect('/login');
    return;
  }

  const category = req.body.category;
  if (!validateCategory(category)) {
    res.render('website', { title: 'Add Website' });
  }
  website.addWebsite(
    req.body.url,
    req.body.title,
    req.body.summary,
    req.body.category);
  res.redirect('/website');
});

module.exports = router;
