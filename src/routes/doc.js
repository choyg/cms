const Uploads = require('../functions/uploads');
const firebase = require('firebase');
const multer = require('multer');
const express = require('express');
const Description = require('../functions/Description').default;
const verifyPdf = require('../functions/verifyPdf').default;


const router = express.Router();
const upload = multer(
  {
    dest: 'uploads/',
    limits: {
      fileSize: 100 * 1024 * 1024, // no larger than 100mb
    },
  },
);

/* GET document upload page. */
router.get('/', (req, res) => {
  const user = firebase.auth().currentUser;
  if (user) {
    return res.render('doc', {
      title: 'Add PDF',
      error: '',
      documentTitle: '',
      documentCategory: '',
      documentSummary: '',
      documentDescriptions: [],
    });
  }
  return res.redirect('/login');
});

/* POST upload a new document + descriptions */
router.post('/', upload.fields([
  { name: 'document', maxCount: 1 },
  { name: 'description', maxCount: 6 }]), (req, res) => {
  const user = firebase.auth().currentUser;
  if (!user) {
    return res.redirect('/login');
  }
  if (!validateCategory(req.body.docCategory)) {
    return res.render('doc', {
      title: 'Add PDF',
      error: 'Invalid Category',
      documentTitle: req.body.docTitle,
      documentCategory: '',
      documentSummary: req.body.docSummary,
      documentDescriptions: [],
    });
  }
  const docFile = req.files.document[0];
  if (!verifyPdf(docFile)) {
    return res.render('doc', {
      title: 'Add PDF',
      error: 'File must be a PDF',
      documentTitle: req.body.docTitle,
      documentCategory: req.body.docCategory,
      documentSummary: req.body.docSummary,
      documentDescriptions: [],
    });
  }
  const descriptions = [];
  if (req.files.description !== undefined) {
    let count = 0;
    req.files.description.forEach((file) => {
      // Verify description file
      if (!verifyPdf(file)) {

      }
      // Create a new Description object and append to array
      let descTitle = req.body[`descTitle${count}`];
      if (!descTitle) {
        descTitle = file.originalname;
      }
      descriptions.push(new Description(descTitle, file));
      count += 1;
    });
  }

  Uploads.uploadPdfResource(
    docFile,
    req.body.docTitle,
    req.body.docSummary,
    req.body.docCategory,
    descriptions);
  return res.redirect('/document');
});

function validateCategory(category) {
  switch (category) {
    case 'training':
      return true;
    case 'safety':
      return true;
    case 'policy':
      return true;
    case 'resources':
      return true;
    case 'checklists':
      return true;
    case 'links':
      return true;
    case 'extras':
      return true;
    case 'miscellaneous':
      return true;
    default:
      return false;
  }
}

module.exports = router;
