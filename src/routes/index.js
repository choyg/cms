var express = require('express');
var router = express.Router();
var firebase = require("firebase");

/* GET home page. */
router.get('/', function (req, res, next) {
    var user = firebase.auth().currentUser;
    if (user) {
        res.render('index', {title: 'Upload'});
    } else {
        res.redirect('/login');
    }
});

module.exports = router;
