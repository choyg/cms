FROM node:slim
WORKDIR /app
COPY package.json package-lock.json ./ 
RUN npm install
COPY . .
RUN apt-get update && apt-get install -y \
	pdftk \
	poppler-utils \
	exiftool \
	imagemagick
EXPOSE 8080	
CMD [ "npm", "start" ]
